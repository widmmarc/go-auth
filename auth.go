package go-auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	DefaultKeycloakDomain = "https://auth.vseth.ethz.ch"
	DefaultRealm          = "VSETH"

	keycloakResponseType = "token+id_token"
	keycloakBasePath     = "/auth/realms/%s/protocol/openid-connect"
	pathCallback         = "/callback"
	pathCacheDir         = ".config/vseth/cli-auth"
	timeout              = time.Minute * 3
)

type Response struct {
	AccessToken string                 `json:"access_token,omitempty"`
	IDToken     string                 `json:"id_token,omitempty"`
	Claims      map[string]interface{} `json:"claims"`
}

type Client struct {
	Name  string
	Ports []int // localports to run the callback server

	KeycloakDomain string // default: DefaultKeycloakDomain
	Realm          string // default: DefaultRealm

	DisableDialog bool
	Insecure      bool
}

// Authenticate the client - using OIDC implict flow. This will start a webserver and open a browser.
// It should only be used for direct user authentication. Not for scripts and service accounts.
func (c *Client) Authenticate(scopes ...string) (resp *Response, err error) {
	// set empty parameters to default (for usage as a go package)
	if c.KeycloakDomain == "" {
		c.KeycloakDomain = DefaultKeycloakDomain
	}
	if c.Realm == "" {
		c.Realm = DefaultRealm
	}

	c.KeycloakDomain, err = secureDomain(c.KeycloakDomain, c.Insecure)
	if err != nil {
		return nil, err
	}

	// check if the token is already cached
	hash := requestHash(c.KeycloakDomain, c.Realm, c.Name, scopes)
	if resp := readCache(hash); resp != nil {
		return resp, nil
	}

	// load the certs to verify the token
	jwks, err := getKeycloakJwks(c.KeycloakDomain, c.Realm)
	if err != nil {
		return nil, err
	}
	nonce, err := generateNonce()
	if err != nil {
		return nil, err
	}

	rspChan := make(chan keycloakResponse)
	errChan := make(chan error)

	keycloakAuthURL := fmt.Sprintf(c.KeycloakDomain + keycloakBasePath + "/auth", c.Realm)
	if strings.Contains(keycloakAuthURL, "%!") {
		return nil, errors.New("invalid keycloakDomain cannot contain any % signs")
	}

	mux := http.NewServeMux()
	// The handle function sends a javascript program that reads the params from
	// the anchor part of the URL and sends them again as json a POST request.
	// We do this because sending it directly as a form from keycloak triggers
	// a warning in browser (sending form over insecure connection). The anchor
	// part has to be read by javascript as it is not sent to the server.
	mux.HandleFunc(pathCallback, func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			w.Write([]byte(html))
		case "POST":
			w.Write([]byte(""))
			var s keycloakResponse
			err := json.NewDecoder(r.Body).Decode(&s)
			if err != nil {
				errChan <- errors.New("error decoding keycloak response" + s.Error)
			} else {
				rspChan <- s
			}
		}
	})

	type tcpKeepAliveListener struct {
		*net.TCPListener
	}
	ln, err := listenerForFirstFreePort(c.Ports...)
	if err != nil {
		return resp, err
	}

	callbackHost := ln.Addr().String()
	srv := &http.Server{Addr: callbackHost, Handler: mux}
	go func() {
		err := srv.Serve(tcpKeepAliveListener{ln.(*net.TCPListener)})
		// Wait for server to exit and close channels
		if err != nil && err != http.ErrServerClosed {
			errChan <- err
		}
		close(rspChan)
		close(errChan)
	}()

	callbackHost = strings.Replace(callbackHost, "127.0.0.1", "http://localhost", 1)
	urlParams := map[string]string{
		"client_id":     c.Name,
		"scope":         strings.Join(scopes, "+"),
		"response_type": keycloakResponseType,
		"redirect_uri":  callbackHost + pathCallback,
		"nonce":         nonce,
	}

	authURL := setURLParams(keycloakAuthURL, urlParams)
	if err := openURL(authURL); err != nil {
		return resp, fmt.Errorf("failed to open authentication URL:\n%s", err.Error())
	}

	if !c.DisableDialog {
		fmt.Printf("%s\nGo to your browser for authentication.\n", authURL)
	}

	// Wait for one of the following
	select {
	case rsp := <-rspChan:
		err = srv.Shutdown(context.Background())
		if err != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", err.Error())
		}
		resp, err := processRPC(rsp, jwks, nonce)
		if err != nil {
			return nil, err
		}
		writeCache(hash, *resp)
		return resp, <-errChan
	case err = <-errChan:
		err = srv.Shutdown(context.Background())
		if err != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", err.Error())
		}
		// We got an error. Something is pretty wrong. Propagate the error
		return resp, fmt.Errorf("%s", err.Error())
	case <-time.After(timeout):
		err = srv.Shutdown(context.Background())
		if err != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", err.Error())
		}
		// We have a timeout. Let's return a timeout error after trying to close the server
		err = <-errChan
		if err != nil {
			return resp, fmt.Errorf("error while shutting down http server after timeout:\n%s", err.Error())
		}
		return resp, fmt.Errorf("timeout while authorizing")
	}
}

// Implements the same authenticator interface as 'Client' but uses the Client Credentials Grant
type ServiceAccountClient struct {
	Username string // Username of SA
	Password string // Password of SA

	KeycloakDomain string // default: DefaultKeycloakDomain
	Realm          string // default: DefaultRealm

	Insecure bool
}

// Authenticate the service account using the Client Credentials Grant
// Note: As of writing this. Keycloak does not seem to support requesting certain scopes and will return
// all roles regardless of the requested scopes.
func (sa *ServiceAccountClient) Authenticate(scopes ...string) (*Response, error) {
	// set empty parameters to default (for usage as a go package)
	if sa.KeycloakDomain == "" {
		sa.KeycloakDomain = DefaultKeycloakDomain
	}
	if sa.Realm == "" {
		sa.Realm = DefaultRealm
	}

	var err error
	sa.KeycloakDomain, err = secureDomain(sa.KeycloakDomain, sa.Insecure)
	if err != nil {
		return nil, err
	}

	// check if the token is already cached
	hash := requestHash(sa.KeycloakDomain, sa.Realm, sa.Username, scopes)
	if resp := readCache(hash); resp != nil {
		return resp, nil
	}

	//load the certs to verify the token
	jwks, err := getKeycloakJwks(sa.KeycloakDomain, sa.Realm)
	if err != nil {
		return nil, err
	}

	keycloakTokenURL := fmt.Sprintf(sa.KeycloakDomain + keycloakBasePath + "/token", sa.Realm)
	if strings.Contains(keycloakTokenURL, "%!") {
		return nil, errors.New("invalid keycloakDomain cannot contain any % signs")
	}

	c := &http.Client{}
	postValues := url.Values{"grant_type": {"client_credentials"}, "scope": scopes}
	req, err := http.NewRequest(http.MethodPost, keycloakTokenURL, strings.NewReader(postValues.Encode()))
	if err != nil {
		return nil, fmt.Errorf("error building request for service account\n %w", err)
	}
	req.SetBasicAuth(sa.Username, sa.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r, err := c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error requesting token for service account\n %w", err)
	}
	defer r.Body.Close()

	raw, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response for service account\n %w", err)
	}

	resp := &Response{}
	err = json.Unmarshal(raw, resp)
	if err != nil {
		return resp, fmt.Errorf("error unmarshaling response for service account\n %w", err)
	}
	if resp.AccessToken == "" {
		return resp, fmt.Errorf("unexpected response from keycloak: %v", string(raw))
	}

	// for the service account we do not get an id token, so we have to parse the
	// access token here to get the claims
	claims, err := parseClaimsFromToken(resp.AccessToken, jwks)
	if err != nil {
		return nil, err
	}
	resp.Claims = claims

	// update the cache
	writeCache(hash, *resp)

	return resp, nil
}

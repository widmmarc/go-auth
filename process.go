package go-auth

import (
	"errors"
	"fmt"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/lestrrat/go-jwx/jwk"
)

type keycloakResponse struct {
	Token   string `json:"access_token"`
	IDToken string `json:"id_token"`
	State   string `json:"session_state"`
	TType   string `json:"token_type"`
	Error   string `json:"error_description"`
}

const (
	msgUnexpectedTokenFormat = "unexpected token format"
)

func processRPC(resp keycloakResponse, jwks *jwk.Set, nonce string) (*Response, error) {
	// check if there is an error
	if resp.Error != "" {
		return nil, fmt.Errorf("keycloak returned with an error:\n%s", resp.Error)
	}

	// parse ID token
	claims, err := parseClaimsFromToken(resp.IDToken, jwks)
	if err != nil {
		return nil, err
	}
	if tokenNonce, ok := claims["nonce"].(string); !ok {
		return nil, errors.New(msgUnexpectedTokenFormat)
	} else if tokenNonce != nonce {
		return nil, errors.New("invalid nonce")
	}

	return &Response{
		AccessToken: resp.Token,
		IDToken:     resp.IDToken,
		Claims:      claims,
	}, nil
}

func parseClaimsFromToken(token string, jwks *jwk.Set) (map[string]interface{}, error) {
	// parse token
	parsedToken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		kid, ok := t.Header["kid"].(string)
		if !ok {
			return nil, errors.New("expected kid in jwt header")
		}
		if res := jwks.LookupKeyID(kid); 0 < len(res) {
			return res[0].Materialize()
		}
		return nil, errors.New("no key for kid")
	})
	// ignore "Token used before issued" error, as client and server are not perfectly in sync
	if err != nil && err.Error() != "Token used before issued" {
		return nil, err
	}
	jwtClaims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New(msgUnexpectedTokenFormat)
	}
	claims := map[string]interface{}(jwtClaims)

	return claims, nil
}

package go-auth

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/lestrrat/go-jwx/jwk"
	"net"
	"net/url"
	"os/exec"
	"runtime"
	"strings"
)

func openURL(url string) error {
	switch runtime.GOOS {
	case "darwin":
		return exec.Command("open", url).Start()
	case "linux":
		if _, err := exec.LookPath("xdg-open"); err != nil {
			// Linux does not always have xdg-open (e.g. in Windows Subsystem for Linux)
			fmt.Printf("Could not launch browser: xdg-open is not present in PATH\n")
			return nil
		}
		return exec.Command("xdg-open", url).Start()
	case "windows":
		return exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	default:
		return fmt.Errorf("%s not supported", runtime.GOOS)
	}
}

func listenerForFirstFreePort(ports ...int) (ln net.Listener, err error) {
	if len(ports) < 1 {
		err = errors.New("no port provided")
		return
	}
	for _, p := range ports {
		addr := fmt.Sprintf("localhost:%v", p)
		ln, err = net.Listen("tcp", addr)
		if err == nil {
			return
		}
	}
	return
}

func setURLParams(url string, params map[string]string) string {
	var buf strings.Builder
	for k, v := range params {
		if buf.Len() == 0 {
			buf.WriteByte('?')
		} else {
			buf.WriteByte('&')
		}
		buf.WriteString(k)
		buf.WriteByte('=')
		buf.WriteString(v)
	}
	return url + buf.String()
}

func generateNonce() (string, error) {
	const nonceLength = 32
	b := make([]byte, nonceLength)
	_, err := rand.Read(b)
	return base64.URLEncoding.EncodeToString(b), err
}

func secureDomain(rawurl string, httpOnly bool) (string, error) {
	preSecure := "https://"
	preInsecure := "http://"
	if !httpOnly && !strings.HasPrefix(rawurl, preSecure) {
		rawurl = strings.TrimPrefix(rawurl, preInsecure)
		rawurl = preSecure + rawurl
	}
	if httpOnly && !strings.HasPrefix(rawurl, preInsecure) {
		rawurl = preInsecure + rawurl
	}
	url, err := url.Parse(rawurl)
	if err != nil {
		return "", err
	}
	if !strings.HasPrefix(url.Host, "localhost") && !strings.HasPrefix(preSecure, url.Scheme) {
		return "", errors.New("insecure (http) can only be used on localhost")
	}
	return rawurl, nil
}

func getKeycloakJwks(domain string, realm string) (*jwk.Set, error) {
	keycloakCertURL := fmt.Sprintf(domain + keycloakBasePath + "/certs", realm)
	return jwk.FetchHTTP(keycloakCertURL)
}
